import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


//Création de 'app' en tant que composant qui renvoi un retour --> élément HTML
class App extends React.Component {

    //OBJET D'ETAT
    state = {

    };

    render() {

        const titre = "TODOList en REACT";
        const tache = "Tache 1"
        const heure = "12:10"

        return <div>
                    <h1>{titre}</h1>

                    <ul>
                        <li><input type="checkbox"/>
                            {tache}
                            {heure}
                        </li>
                    </ul>

                    <div>
                        <p>Filtrer la liste de taches :</p>
                        <p>
                            <input type="checkbox" />
                            <label>Toutes</label>
                        </p>
                        <p>
                            <input type="checkbox" />
                            <label>Non Réalisées</label>
                        </p>
                        <p>
                            <input type="checkbox" />
                            <label>Réalisées</label>
                        </p>
                    </div>
                    
                    <form> 
                        <div>
                            <label>Nom : </label>
                            <input  type="text"  
                                    onChange={this.nomTache}  
                                    placeholder="Nom de la tache"/>

                        </div>
                        <div>
                            <label>Heure : </label>
                            <input  type="text"  
                                onChange={this.heureDate}  
                                placeholder="heure de la tache"/>
                        </div>
                        <div>
                            <input type="submit"  value="Enregistrer"/>
                        </div>
                    </form>
                </div>
    };
}

const rootElement = document.getElementById('root'); //creation var de type 'rootElement' qui est égale à id root

//demande de renvoi vers le DOM de React
ReactDOM.render(<App />, rootElement);